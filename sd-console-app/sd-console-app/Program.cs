﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace sd_console_app
{
    class Program
    {
        private const string HOST = "http://127.0.0.1";
        private static string path = @"C:\Users\Felipe\Downloads\sistemas distribuidos\projeto2\sd-projeto-2\sd-dht-api\sd-dht-api\bin\Release\netcoreapp2.1\win-x64\sd-dht-api.exe";

        static void Main(string[] args)
        {
            while (!File.Exists(path))
            {
                Console.WriteLine("\nExecutável nao encontrado. Digite o caminho:");
                path = Console.ReadLine();
            }

            var processes = new Process[10];

            try
            {
                Console.WriteLine("Abrindo 10 nós.");
                for (int indice = 0; indice < 10; ++indice)
                {
                    var port = (indice * 100) + 56000;
                    processes[indice] = Process.Start(path, $"{HOST}:{port}");
                    Console.WriteLine($"#{indice} Nó aberto: {HOST}:{port}");
                }

                Console.WriteLine($"\nPara consultar o status do nó: {HOST}:<porta>/api/externo/status");
                Console.WriteLine($"Para solicitar JOIN na rede..: {HOST}:<porta>/api/externo/join/ip/porta");
                Console.WriteLine($"Para solicitar LEAVE na rede.: {HOST}:<porta>/api/externo/leave\n");
            }
            catch (Exception ex)
            {
                foreach (var process in processes)
                    process?.Kill();

                Console.WriteLine($"Erro ao iniciar os processos. Erro {ex.Message}.");
            }
            finally
            {
                Console.Read();
            }
        }
    }
}
