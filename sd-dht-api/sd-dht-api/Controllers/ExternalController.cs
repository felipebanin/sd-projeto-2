﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using sd_dht_api.Models;

namespace sd_dht_api.Controllers
{
    /// <summary>
    /// Classe controller que gerencia as requisições que o usuário pode realizar.
    /// </summary>
    [Route("api/externo")]
    [ApiController]
    public class ExternalController : ControllerBase
    {
        private readonly State state;

        /// <summary>
        /// Construtor padrão que recebe a injeção de depência para possuir um estado.
        /// </summary>
        /// <param name="state">Encapsulamento das informações necessário para manter um estado.</param>
        public ExternalController(State state)
        {
            this.state = state;
        }

        /// <summary>
        /// Consulta o estado do nó que à requisita.
        /// </summary>
        /// <returns>JSON com as informações do estado do nó.</returns>
        [Produces("application/json")]
        [HttpGet("status")]
        public IActionResult Status()
            => new JsonResult(this.state, new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include
            });

        /// <summary>
        /// Solicita entrada na rede DHT.
        /// </summary>
        /// <param name="ip">Ip de um nó que está na rede.</param>
        /// <param name="port">Porta do nó que escuta o Ip.</param>
        /// <returns>Retorna o status da requisição de Join ao nó fornecido.
        /// Este retorno não é o suficiente para garantir o sucesso ou a falha
        /// no processo de entrar na rede.</returns>
        [Produces("text/plain")]
        [HttpGet("join/{ip}/{port}")]
        public async Task<string> Join(string ip, int port)
        {
            // atualizo as informações sobre este nó.
            this.state.Me.IP = this.HttpContext.Connection.LocalIpAddress.ToString();
            this.state.Me.Port = this.HttpContext.Connection.LocalPort;

            // verifico se já não está conectado a rede.
            if (this.state.IsConnected)
                return "Você já está conectado.";

            // construo a url para solicitar o join.
            var uriRequest = new StringBuilder();
            uriRequest.Append($"http://{ip}:{port}/api/interno/join_request/");
            uriRequest.Append($"{this.state.Me.Identify}/{this.state.Me.IP}/{this.state.Me.Port}");

            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(uriRequest.ToString());

                    return $"Requisição enviada. HttpStatusCode: {response.StatusCode}.";
                }
            }
            catch (Exception ex)
            {
                return $"Falha na requisição. Mensagem de erro: {ex.Message}.";
            }
        }

        /// <summary>
        /// Solicita saída da rede DHT.
        /// </summary>
        /// <returns>Retorna o status das requisições de Leave.
        /// Este retorno não é o suficiente para garantir o sucesso ou a falha
        /// no processo de sair da rede.</returns>
        [Produces("text/plain")]
        [HttpGet("leave")]
        public async Task<string> Leave()
        {
            if (!this.state.IsConnected)
                return "Você não está conectado";

            // faz seu antecessor apontar para seu sucessor.
            var uriPreviousToNext = new StringBuilder();
            uriPreviousToNext.Append($"http://{this.state.Previous.IP}:{this.state.Previous.Port}/");
            uriPreviousToNext.Append($"api/interno/atualiza_sucessor/");
            uriPreviousToNext.Append($"{this.state.Next.Identify}/{this.state.Next.IP}/{this.state.Next.Port}");

            // faz seu sucessor(previous) apontar para seu antecessor.
            var uriNextToPrevious = new StringBuilder();
            uriNextToPrevious.Append($"http://{this.state.Next.IP}:{this.state.Next.Port}/");
            uriNextToPrevious.Append($"api/interno/atualiza_antecessor/");
            uriNextToPrevious.Append($"{this.state.Previous.Identify}/{this.state.Previous.IP}/{this.state.Previous.Port}");

            try
            {
                HttpResponseMessage response0, response1;
                using (var client = new HttpClient())
                {
                    response0 = await client.GetAsync(uriPreviousToNext.ToString());
                    response1 = await client.GetAsync(uriNextToPrevious.ToString());
                }

                this.state.Next = null;
                this.state.Previous = null;

                return $"Requisição enviada. HttpStatusCode: {response0.StatusCode}, {response1.StatusCode}.";
            }
            catch (Exception ex)
            {
                return $"Falha na requisição. Mensagem de erro: {ex.Message}.";
            }
        }
    }
}