﻿using Microsoft.AspNetCore.Mvc;
using sd_dht_api.Models;
using System.Net.Http;
using System.Text;

namespace sd_dht_api.Controllers
{
    /// <summary>
    /// Classe controller que gerencia as requisições que fazem a manutenção interna da rede.
    /// </summary>
    [Route("api/interno")]
    [ApiController]
    public class InternalController : ControllerBase
    {
        private readonly State state;

        /// <summary>
        /// Construtor padrão que recebe a injeção de depência para possuir um estado.
        /// </summary>
        /// <param name="state">Encapsulamento das informações necessário para manter um estado.</param>
        public InternalController(State state)
        {
            this.state = state;
        }

        [HttpGet("join_request/{sourceId}/{sourceIp}/{sourcePort}")]
        public async void JoinRequest(int sourceId, string sourceIp, int sourcePort)
        {
            // primeiro join do sistema
            if (!this.state.IsConnected)
            {
                this.state.Me.IP = HttpContext.Connection.LocalIpAddress.ToString();
                this.state.Me.Port = HttpContext.Connection.LocalPort;
                this.state.Next = new Node(sourceId, sourceIp, sourcePort);
                this.state.Previous = new Node(sourceId, sourceIp, sourcePort);

                var uriSetPrevious = new StringBuilder();
                uriSetPrevious.Append($"http://{sourceIp}:{sourcePort}/");
                uriSetPrevious.Append($"api/interno/atualiza_antecessor/");
                uriSetPrevious.Append($"{this.state.Me.Identify.ToString()}/{this.state.Me.IP}/");
                uriSetPrevious.Append($"{this.state.Me.Port.ToString()}");

                var uriSetNext = new StringBuilder();
                uriSetNext.Append($"http://{sourceIp}:{sourcePort}/");
                uriSetNext.Append($"api/interno/atualiza_sucessor/");
                uriSetNext.Append($"{this.state.Me.Identify.ToString()}/{this.state.Me.IP}/");
                uriSetNext.Append($"{this.state.Me.Port.ToString()}");

                using (var client = new HttpClient())
                {
                    var responsePrevious = await client.GetAsync(uriSetPrevious.ToString());
                    var responseNext = await client.GetAsync(uriSetNext.ToString());
                }
            }

            // encontrou o lugar correto
            else if ((this.state.Me.Identify < sourceId && this.state.Next.Identify > sourceId) || // nó entre maior e menor
                (this.state.Next.Identify < this.state.Me.Identify &&
                (sourceId < this.state.Next.Identify || // menor nó da rede
                sourceId > this.state.Me.Identify))) // maior nó da rede
            {
                // atualiza o this.next qual será o seu novo antecessor.
                var uriOldNext = new StringBuilder();
                uriOldNext.Append($"http://{this.state.Next.IP}:{this.state.Next.Port}/");
                uriOldNext.Append($"api/interno/atualiza_antecessor/{sourceId.ToString()}/{sourceIp}/{sourcePort}");

                // atualiza o nó(source) que pediu o join com o novo next dele (que é this.state.NextNode)
                var baseUrlSourceNext = new StringBuilder();
                baseUrlSourceNext.Append($"http://{sourceIp}:{sourcePort}/");
                baseUrlSourceNext.Append($"api/interno/atualiza_sucessor/{this.state.Next.Identify.ToString()}/");
                baseUrlSourceNext.Append($"{this.state.Next.IP}/{this.state.Next.Port}");

                // atualiza o nó(source) que pediu o join com o novo next dele (que é this.state.Me)
                var baseUrlSourcePrevious = new StringBuilder();
                baseUrlSourcePrevious.Append($"http://{sourceIp}:{sourcePort}/");
                baseUrlSourcePrevious.Append($"api/interno/atualiza_antecessor/{this.state.Me.Identify.ToString()}/");
                baseUrlSourcePrevious.Append($"{this.state.Me.IP}/{this.state.Me.Port}");

                using (var client = new HttpClient())
                {
                    var response0 = await client.GetAsync(uriOldNext.ToString());
                    var response1 = await client.GetAsync(baseUrlSourceNext.ToString());
                    var response2 = await client.GetAsync(baseUrlSourcePrevious.ToString());
                }

                // atualizo meu state.
                this.state.Next.Identify = sourceId;
                this.state.Next.IP = sourceIp;
                this.state.Next.Port = sourcePort;
            }

            // repassa o join_request
            else
            {
                var baseUrl = new StringBuilder();
                baseUrl.Append($"http://{this.state.Next.IP}:{this.state.Next.Port}/");
                baseUrl.Append($"api/interno/join_request/{sourceId}/{sourceIp}/{sourcePort}");
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(baseUrl.ToString());
                }
            }
        }

        [HttpGet("atualiza_sucessor/{newNextId}/{newNextIp}/{newNextPort}")]
        public IActionResult SetNextNode(int newNextId, string newNextIp, int newNextPort)
        {
            if (this.state.Next == null)
                this.state.Next = new Node();

            if (this.state.Me.Identify == newNextId)
            {
                this.state.Next = null;
            }
            else
            {
                this.state.Next.Identify = newNextId;
                this.state.Next.IP = newNextIp;
                this.state.Next.Port = newNextPort;
            }

            return Ok();
        }

        [HttpGet("atualiza_antecessor/{newPreviousId}/{newPreviousIp}/{newPreviousPort}")]
        public IActionResult SePreviousNode(int newPreviousId, string newPreviousIp, int newPreviousPort)
        {
            if (this.state.Previous == null)
                this.state.Previous = new Node();

            if (this.state.Me.Identify == newPreviousId)
            {
                this.state.Previous = null;
            }
            else
            {
                this.state.Previous.Identify = newPreviousId;
                this.state.Previous.IP = newPreviousIp;
                this.state.Previous.Port = newPreviousPort;
            }

            return Ok();
        }
    }
}
