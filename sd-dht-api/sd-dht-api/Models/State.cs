﻿using Newtonsoft.Json;
using System;

namespace sd_dht_api.Models
{
    /// <summary>
    /// Entidade que guarda o estado do nó.
    /// </summary>
    public class State
    {
        [JsonProperty(PropertyName = "eu")]
        public Node Me { get; set; }

        [JsonProperty(PropertyName = "sucessor")]
        public Node Next { get; set; }

        [JsonProperty(PropertyName = "antecessor")]
        public Node Previous { get; set; }

        [JsonProperty(PropertyName = "conectado")]
        public bool IsConnected
        {
            get => (this.Next != null && this.Previous != null);
        }

        public State()
        {
            this.Me = new Node
            {
                Identify = new Random().Next(1, int.MaxValue)
            };
        }
    }
}
