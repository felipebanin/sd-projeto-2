﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace sd_dht_api.Models
{
    /// <summary>
    /// Entidade anemica de nó da rede.
    /// </summary>
    public class Node
    {
        [JsonProperty(PropertyName = "identificador")]
        [FromQuery(Name = "identificador")]
        public int Identify { get; set; }

        [JsonProperty(PropertyName = "ip")]
        [FromQuery(Name = "ip")]
        public string IP { get; set; }

        [JsonProperty(PropertyName = "porta")]
        [FromQuery(Name = "porta")]
        public int Port { get; set; }

        public Node() { }

        public Node(Node node) : this(node.Identify, node.IP, node.Port) { }

        public Node(int id, string ip, int port)
        {
            this.Identify = id;
            this.IP = ip;
            this.Port = port;
        }
    }
}
